Quick serve, eight-seat omakase sushi counter at Sanctuary Hotel offering a single 12-piece menu.

Address: 132 W 47th St, New York, NY 10036, USA

Phone: 917-348-5737
